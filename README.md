### Ruby script(s) and Ansible playbooks for setting up Ubuntu-based WordPress host(s) on Digital Ocean.


# Setup

To use ansible:

* sudo pip install ansible

To use recreate.vm:

* gem install awesome_print
* gem install droplet_kit


# Instructions:


* If using Digital Ocean:
** Run `./recreate-vm.rb`, then run `ssh root@do` to activate WordPress.
* else
** Ensure you have ssh access to root without password
* end

Run playbooks:

* Run: `./run-all-playbooks`
* Run: `sudo apt-get install phpmyadmin`
* Add this line to .zshrc to get host name in prompt: export PS1="`hostname`: $PS1"
* Add public key to github to be able to git clone repos.

