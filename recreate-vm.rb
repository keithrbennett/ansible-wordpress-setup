#!/usr/bin/env ruby

# Creates a Digital Ocean WordPress instance ("droplet").
# Options should be in the form of a hash expressed as YAML
# (without the #'s):
# ---
# :floating_ip: 138.197.253.55
# :host_aliases:
# - blog
# - do
# :region: NYC2


require 'awesome_print'
require 'droplet_kit'
require 'optparse'
require 'ostruct'
require 'shellwords'
require 'trick_bag'
require 'yaml'


class VmRecreator

  DIGOCEAN_TOKEN = ENV['DIGOCEAN_API_TOKEN']

  REQUIRED_KEYS = %w(
      blog_title
      blog_sql_file
      blog_url
      host_name
      host_aliases
      size
      region
      floating_ip
      ssh_keys
  )

  attr_reader :client, :options

  # Get command line arguments, one of which is a YAML config filespec.
  # Read that file and return an OpenStruct with its settings.
  # File should be YAML representation of a hash such as this:
  # PROD_OPTIONS = {
  #       host_name: 'bbs-wordpress',
  #       host_aliases: %w(tblog tdo),
  #       size: '1gb',
  #       region: 'NYC2',
  #       floating_ip: '138.197.253.55',
  #       ssh_keys: %w(
  # 	ed:1c:68:ee:fb:35:b8:60:c0:ee:24:e5:64:7c:b2:58
  # 	2e:a4:01:93:a5:dd:0d:1b:65:36:79:c3:07:ac:6e:2f
  # )}


  def get_run_options
    args = {}
    usage = "Usage: recreate_vm -c config_filespec"

    OptionParser.new do |opts|
      opts.banner = usage

      opts.on("-c", "--config [FILESPEC]", "Configuration filespec") do |value|
        args[:config_filespec] = value
      end
    end.parse!

    unless args[:config_filespec]
      puts "Must specify config file!: #{usage}"
      exit(-1)
    end

    options_hash = YAML.load_file(args[:config_filespec])
    TrickBag::Validations.raise_on_missing_keys(options_hash, REQUIRED_KEYS)
    OpenStruct.new(options_hash)
  end


  def initialize
    @client = create_dig_ocean_client
    @options = get_run_options
  end


  # This creates the client instance but does not contact the server, so authentication errors
  # will not be checked here.
  def create_dig_ocean_client
    unless DIGOCEAN_TOKEN
      raise "Must have Digital Ocean API token defined in environment variable DIGOCEAN_API_TOKEN."
    end
    DropletKit::Client.new(access_token: DIGOCEAN_TOKEN)
  end


  def wait_for_droplet_status(droplet, target_value = 'active')
    start_time = Time.now
    text_generator = -> { "%4d  State: %s" % [Time.now - start_time, droplet.status] }
    updater = TrickBag::Io::TextModeStatusUpdater.new(text_generator)
    loop do
      droplet = @client.droplets.find(id: droplet.id)
      updater.print
      if droplet.status == target_value
        puts
        return
      end
      sleep 0.3
    end
    puts "Sleeping another 5 seconds..."; sleep 5
  end


  def create_wp_droplet

    wordpress_droplet_config = {
        name:     options.host_name,
        region:   options.region,
        image:    wordpress_image_id,
        size:     options.size,
        ssh_keys: options.ssh_keys,
        # Note: A command like this will yield the format needed here: ssh-keygen -l -E MD5  -f id_rsa.pub
        # ssh_keys: ['ed:1c:68:ee:fb:35:b8:60:c0:ee:24:e5:64:7c:b2:58', \
        #            '2e:a4:01:93:a5:dd:0d:1b:65:36:79:c3:07:ac:6e:2f' ]
    }
    new_droplet_template = DropletKit::Droplet.new(wordpress_droplet_config)
    new_droplet = @client.droplets.create(new_droplet_template)
    puts "Created new droplet #{new_droplet.id}. Waiting for active state..."
    wait_for_droplet_status(new_droplet)
    sleep 5
    new_droplet
  end


  def wordpress_image_id
    word_press_images = @client.images.all.select { |image| image.slug == 'wordpress-16-04' }
    word_press_images.last.id
  end


  def list_droplets
    @client.droplets.all.map(&:to_h).each { |d| ap d }
  end


  # Run command, outputting both stdout and stderr when it is done
  def run_command(command)
    puts command
    output = `#{command} 2>&1`
    puts output
    output
  end


  def reset_floating_ip(ip, new_droplet)

    start_time = Time.now
    text_generator = -> { "%4d" % [Time.now - start_time] }
    updater = TrickBag::Io::TextModeStatusUpdater.new(text_generator)

    get_floating_ip = -> { @client.floating_ips.find(ip: ip) }

    wait_for_floating_ip_assignment = -> do
      loop do
        fip = get_floating_ip.()
        if fip.droplet
          puts
          return fip
        end
        updater.print
        sleep 0.3
      end
    end


    # @client.floating_ip_actions.unassign(ip: ip)
    @client.floating_ip_actions.assign(ip: ip, droplet_id: new_droplet.id)
    puts "Assigning floating ip #{ip} to droplet at #{new_droplet.id}. Waiting for completion..."
    wait_for_floating_ip_assignment.()
    puts "Assigned floating ip #{ip} to droplet at IP address #{get_floating_ip.().droplet.public_ip}"
  end


  def remove_vm(floating_ip_addr)
    floating_ip = @client.floating_ips.find(ip: floating_ip_addr)

    # Might be: {"id":"unauthorized","message":"Unable to authenticate you."}
    if floating_ip.is_a?(String)
      raise "Digital Ocean API Error: #{floating_ip}"
    end

    if floating_ip.droplet
      droplet_id = floating_ip.droplet.id
      puts "\n\nRemoving droplet with id #{droplet_id}"
      @client.droplets.delete(id: droplet_id)
      puts "Removed droplet at #{floating_ip_addr}"
    else
      puts "No droplet associated with floating IP #{floating_ip_addr}. Not deleting any VM."
    end
  end


  def update_known_hosts

    run_ssh_keyscan = ->(host) do
      tries = 15
      (1..tries).each do |i|
        lines = run_command("ssh-keyscan -t rsa #{host} 2>&1").split("\n")
        return lines if lines.any?
        if i < tries; puts "No key for host #{host} available yet, trying again... " end
      end
      puts "Unable to find ssh keys for host #{host}"
      []
    end

    aliases = options.host_aliases + Array(options.floating_ip)
    known_hosts_filespec = File.join(ENV['HOME'], '.ssh', 'known_hosts')
    puts

    aliases.each do |host_spec|

      puts "Attempting to remove #{host_spec} from known_hosts file..."
      run_command("ssh-keygen -R #{host_spec}")

      puts "Attempting to register new key for #{host_spec} in known_hosts file..."
      lines = run_ssh_keyscan.(host_spec)

      # require 'pry'; binding.pry

      good_lines, bad_lines = lines.partition { |line| line.split[1] == 'ssh-rsa' }
      if bad_lines.any?
        puts "\nBad lines not appended to known_hosts:"; puts bad_lines
      end
      puts "\nGood lines appended to known_hosts:"; puts good_lines
      File.open(known_hosts_filespec, 'a') { |f| f << good_lines.join("\n") << "\n" }
    end
  end


  def call
    remove_vm(options.floating_ip)
    new_droplet = create_wp_droplet
    reset_floating_ip(options.floating_ip, new_droplet)
    update_known_hosts
    puts "Done with recreate-vm."
  end
end


VmRecreator.new.call
