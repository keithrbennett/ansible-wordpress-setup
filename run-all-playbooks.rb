#!/usr/bin/env ruby

# Runs all playbooks required to set up the WordPress host.

require 'open3'
require 'optparse'
require 'ostruct'


class RunAllPlaybooks

  SEPARATOR_LINE = "#{'*' * 79}\n"

  START_TIME = Time.now

  attr_reader :options, :playbook_specs

  def get_run_options
    args = {}
    usage = "Usage: ./run-all-playbooks -e environment"

    OptionParser.new do |opts|
      opts.banner = usage

      opts.on("-e", "--environment=test-prod-etc", "test, prod, etc.") do |value|
        args[:environment] = value
      end
    end.parse!

    unless args[:environment]
      puts usage
      puts "Environment (test, prod, etc.) not specified; using test."
      args[:environment] = 'test'
    end

    OpenStruct.new(args)
  end


  def initialize
    @options = get_run_options
    playbook_filemask = File.join(File.dirname(__FILE__), 'playbooks', '*{yml,yaml}')
    @playbook_specs = Dir[playbook_filemask].sort
  end


  def run_shell_command(command)
    Open3.popen2e(command) do |stdin, stdout_err, wait_thr|
      while line = stdout_err.gets
        puts line
      end

      exit_status = wait_thr.value
      unless exit_status.success?
        abort "!!! FAILED! Command: #{command}, Exit Code: #{exit_status}"
      end
    end
  end


  def run_playbook(playbook_spec)
    puts "\n\n#{SEPARATOR_LINE}Running playbook #{playbook_spec}\n#{SEPARATOR_LINE}\n"
    ini_filespec = "#{options.environment}.ini"
    config_filespec = "../#{options.environment}.yaml"
    playbook_start_time = Time.now
    command = "ansible-playbook -i #{ini_filespec} #{playbook_spec} --extra-vars config=#{config_filespec}"
    puts "Running cmd: #{command}"
    run_shell_command(command)
    puts "\n\nFinished with playbook #{playbook_spec}. " <<
             "Duration was #{Time.now - playbook_start_time} seconds."
  end


  def call
    playbook_specs.each do |playbook|
      run_playbook(playbook)
    end

    puts SEPARATOR_LINE
    puts "Entire operation duration: #{Time.now - START_TIME} seconds"
  end
end


RunAllPlaybooks.new.call
